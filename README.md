# Demo Repo of divergent behavior in Separate Compilation Linking with CMake between Linux and Windows

This example builds on the [parallel-forall repo][1] separate compilation example by adding CMake to it, and shows how behavior differs in CMake's use of device linking code between Windows MSVC and Linux.

Uncomment line 21 in `windows-broken/CMakeLists.txt` to "fix" it on Windows.

## Compile and launch

```bash
git clone https://eloupes_jay@bitbucket.org/eloupes_jay/cuda-sep.git cuda-sep
cd cuda-sep
mkdir build && cd build
cmake -DCMAKE_CUDA_FLAGS="-arch=sm_60" ..
cmake --build .
```

Please change *-arch=sm_60* according to [your GPU compute capability][2].

[1]: https://github.com/parallel-forall/code-samples/tree/master/posts/separate-compilation-linking
[2]: https://en.wikipedia.org/wiki/CUDA#GPUs_supported
